package com.twuc.webApp.controller;

import com.twuc.webApp.domain.GameInformation;
import com.twuc.webApp.domain.GameRequest;
import com.twuc.webApp.domain.GameResult;
import com.twuc.webApp.exception.GameNotExistException;
import com.twuc.webApp.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class GuessNumberGameController {

    private GameService gameService;

    public GuessNumberGameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/games")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity create() throws URISyntaxException {
        Integer id = gameService.createGame();
        return ResponseEntity.created(new URI(String.format("/api/games/%d", id))).build();
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity<GameInformation> getGame(@PathVariable Integer gameId) {
        GameInformation gameInformation = gameService.getAnswer(gameId);
        return ResponseEntity.ok(gameInformation);
    }

    @PatchMapping("/games/{gameId}")
    public ResponseEntity<GameResult> guess(@PathVariable Integer gameId, @RequestBody @Valid GameRequest gameRequest) {
        return ResponseEntity.ok(gameService.guess(gameService.getAnswer(gameId).getAnswer(), gameRequest.getAnswer()));
    }

    @ExceptionHandler({GameNotExistException.class})
    public ResponseEntity handleException() {
        return ResponseEntity.notFound().build();
    }


}
