package com.twuc.webApp.service;

public interface GameGenerator {
    public String generateAnswer();
}
