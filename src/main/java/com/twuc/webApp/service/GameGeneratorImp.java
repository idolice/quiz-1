package com.twuc.webApp.service;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@Component
public class GameGeneratorImp implements GameGenerator {

    public String generateAnswer() {
        Random random = new Random();
        Set<Integer> set = new HashSet<>();
        while (set.size() < 4) {
            set.add(random.nextInt(10));
        }
        return new ArrayList<>(set)
                .stream()
                .map(item -> String.valueOf(item))
                .reduce("", (partial, item) -> partial.concat(item));
    }
}
