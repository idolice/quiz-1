package com.twuc.webApp.service;

import com.twuc.webApp.domain.GameInformation;
import com.twuc.webApp.domain.GameResult;
import com.twuc.webApp.exception.GameNotExistException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameService {
    private static final String template = "%dA%dB";
    private HashMap<Integer, String> ids = new HashMap<>();

    private GameGenerator gameGenerator;

    public GameService(GameGenerator gameGenerator) {
        this.gameGenerator = gameGenerator;
    }

    public GameResult guess(String actual, String guessNum) {
        List<Integer> actualArray = parseAnswer(actual);
        List<Integer> guessArray = parseAnswer(guessNum);
        int positionMatch = 0;

        for (int i = 0; i < 4; i++) {
            if (actualArray.get(i) == guessArray.get(i)) {
                positionMatch++;
            }
        }
        Integer numMatch = new Long(actualArray.stream().filter(guessArray::contains
        ).count()).intValue() - positionMatch;

        GameResult gameResult = new GameResult();
        gameResult.setCorrect(positionMatch == 4);
        gameResult.setHint(String.format(template, positionMatch, numMatch));

        return gameResult;
    }

    public Integer createGame() {
        ids.put(ids.size() + 1, gameGenerator.generateAnswer());
        return ids.size();
    }

    public GameInformation getAnswer(Integer gameId) {
        if (!ids.containsKey(gameId)) {
            throw new GameNotExistException();
        }
        GameInformation gameInformation = new GameInformation();
        gameInformation.setId(gameId);
        gameInformation.setAnswer(ids.get(gameId));
        return gameInformation;
    }

    private List<Integer> parseAnswer(String actual) {
        return actual.chars().boxed().collect(Collectors.toList());
    }


}
