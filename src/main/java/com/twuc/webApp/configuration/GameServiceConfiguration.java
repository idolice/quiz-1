package com.twuc.webApp.configuration;

import com.twuc.webApp.service.GameGenerator;
import com.twuc.webApp.service.MockGameGeneratorImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
public class GameServiceConfiguration {

    @Bean
    @Profile("test")
    @Primary
    public GameGenerator mockGameService(){
        return new MockGameGeneratorImp();
    }
}
