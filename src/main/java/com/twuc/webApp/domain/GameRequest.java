package com.twuc.webApp.domain;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class GameRequest {
    @Pattern(regexp = "[0-9]+")
    @Size(max = 4, min = 4)
    private String answer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
