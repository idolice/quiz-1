package com.twuc.webApp.controller;

import com.twuc.webApp.service.GameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class GuessNumberGameControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    GameGenerator gameGenerator;


    @Test
    void should_create_a_game() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().isCreated());
    }

    @Test
    void should_return_game_uri() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(header().string("Location", "/api/games/1"));
    }

    @Test
    void should_return_game_status() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(get("/api/games/1"))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.answer").value("5893"));
    }

    @Test
    void should_return_404_when_id_not_exist() throws Exception {
        mockMvc.perform(get("/api/games/1"))
                .andExpect(status().is(404));
    }


    @Test
    void should_can_guess_number() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(patch("/api/games/1")
                .content("{ \"answer\": \"5792\" }").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.hint").value("2A0B"))
                .andExpect(jsonPath("$.correct").value(false));
    }

    @Test
    void should_can_guess_number_correct() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(patch("/api/games/1")
                .content("{ \"answer\": \"5893\" }").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.hint").value("4A0B"))
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test
    void should_return_400_when_request_not_correct_format() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(patch("/api/games/1")
                .content("{ \"answer\": \"58935\" }").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_404_when_patch_unexist_game() throws Exception {
        mockMvc.perform(patch("/api/games/15")
                .content("{ \"answer\": \"5895\" }").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(404));
    }
}
